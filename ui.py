import tkinter
import numpy as np
import datetime
import pickle
import pycountry
import model_predict
from tkcalendar import Calendar, DateEntry

screen = tkinter.Tk(className='Dakshit')
screen.title('HR')
screen.geometry('640x480')

frame1 = tkinter.Frame(screen)
frame1.grid(row=0, column=0, padx=(50,50), pady=(30,0), stick='e')

industries = pickle.load(open('Industries.sav', 'rb'))
industry_var = tkinter.StringVar(screen)
tkinter.Label(frame1, text='Industry:').grid(row=0, column=0, pady=(20,0), sticky='w')
industry = tkinter.OptionMenu(frame1, industry_var, *industries)
industry.grid(row=0, column=1, pady=(20,0), sticky='ew')

tkinter.Label(frame1, text='Current Salary:', anchor='w').grid(row=2, column=0, pady=(10,0), sticky='w')
curr_sal = tkinter.Entry(frame1, bd=3, width=20)
curr_sal.grid(row=2, column=1, pady=(10,0))

tkinter.Label(frame1, text='Commute Distance', anchor='w').grid(row=3, column=0, pady=(10,0), sticky='w')
distance = tkinter.Entry(frame1, bd=3, width=20)
distance.grid(row=3, column=1, pady=(10,0))

def call_dob():
    def print_date():
        global dob
        dob = cal.selection_get()
        date.config(text=str(dob))
        top.destroy()
    top = tkinter.Toplevel(screen)
    cal = Calendar(top, font='Arial 14', selectmode='day', cursor='hand1')
    cal.grid()
    tkinter.Button(top, text='OK', command=print_date).grid(row=1)
def dob_text():
    try:
        return dob
    except:
        return 'Select Date'
tkinter.Label(frame1, text='Date of Birth:', anchor='w').grid(row=4, column=0, pady=(10,0), sticky='w')
date = tkinter.Button(frame1, text=dob_text(), anchor='w', width=17, command=lambda: call_dob())
date.grid(row=4, column=1, pady=(10,0))

countries = pickle.load(open('Countries.sav', 'rb'))
country_var = tkinter.StringVar(screen)
tkinter.Label(frame1, text='Country:', anchor='w').grid(row=2, column=2, padx=(20,0), pady=(10,0), sticky='w')
country = tkinter.OptionMenu(frame1, country_var, *countries)
country.grid(row=2, column=3, pady=(10,0), padx=(0,50), sticky='ew')

def call_doh():
    def print_date():
        global doh
        doh = (cal.selection_get())
        hire.config(text=str(doh))
        top.destroy()
    top = tkinter.Toplevel(screen)
    cal = Calendar(top, font='Arial 14', selectmode='day', cursor='hand1')
    cal.grid()
    tkinter.Button(top, text='OK', command=print_date).grid(row=1)
def hiring_text():
    try:
        return doh
    except:
        return 'Select Date'
tkinter.Label(frame1, text='Date of Hiring:', anchor='w').grid(row=4, column=2, padx=(20,0), pady=(10,0), sticky='w')
hire = tkinter.Button(frame1, text=hiring_text(), anchor='w', width=17, command=lambda: call_doh())
hire.grid(row=4, column=3, pady=(10,0), padx=(0,50))

gender_var = tkinter.StringVar()
gender_var.set(None)
tkinter.Label(frame1, text='Gender:', anchor='w').grid(row=5, column=0, pady=(10,0), sticky='w')
tkinter.Radiobutton(frame1, text='Male', variable=gender_var, value='Male').grid(row=5, column=1, sticky='w', pady=(10,0))
tkinter.Radiobutton(frame1, text='Female', variable=gender_var, value='Female').grid(row=6, column=1, sticky='w', pady=(0,0))

tkinter.Label(screen, text='Prediction:', anchor='w').grid(row=2, column=0, pady=(10,0), padx=(50,0), sticky='w')
prediction_display = tkinter.Label(screen, text='-')
prediction_display.grid(row=2, column=0, pady=(10,0))

tkinter.Label(screen, text='Confidence of Employee:', anchor='w').grid(row=3, column=0, pady=(10,0), padx=(30,0), sticky='w')
confidence_display = tkinter.Label(screen, text='-')
confidence_display.grid(row=3, column=0, pady=(10,0))

tkinter.Label(screen, text='Log Confidence of Employee:', anchor='w').grid(row=4, column=0, pady=(10,0), padx=(30,0), sticky='w')
log_confidence_display = tkinter.Label(screen, text='-')
log_confidence_display.grid(row=4, column=0, pady=(10,0))

def call_model():
    categories = list()
    categories.append(industry_var.get())
    categories.append(gender_var.get())
    categories.append(pycountry.countries.lookup(country_var.get()).alpha_2)
    duration = datetime.date.today() - doh
    duration = duration.days
    categories.append(duration)
    age = datetime.date.today().year - dob.year
    categories.append(age)
    categories.append(curr_sal.get())
    categories.append(distance.get())
    prediction, confidence, log_confidence = model_predict.predict(np.asarray(categories))
    print(prediction, confidence, log_confidence)
    prediction_display.config(text=prediction)
    confidence_display.config(text=confidence)
    log_confidence_display.config(text=log_confidence)

tkinter.Button(screen, text='Submit', width=8, command=call_model).grid(row=1, column=0, pady=(30,0), columnspan=4)

screen.mainloop()
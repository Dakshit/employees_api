import pandas as pd  
import numpy as np  
import matplotlib.pyplot as plt
import sklearn
import datetime
import math
import pycountry
from sklearn.model_selection import train_test_split 
from sklearn.linear_model import LinearRegression
from sklearn import metrics, preprocessing, linear_model, neighbors, svm
import pickle

# education_array, job_array, employee_array, gender_array, ethnicity_array, department_array, nationality_array, duration_array, age_array, salary_array

def predict(categories):
    file_handler = open('knn_model.sav', 'rb')
    clf_knn = pickle.load(file_handler)
    file_handler.close()

    industry_one_hot = pickle.load(open('Industry_one_hot.sav', 'rb'))
    gender_one_hot = pickle.load(open('Gender_one_hot.sav', 'rb'))
    country_one_hot = pickle.load(open('Country_one_hot.sav', 'rb'))

    duration_scaler = pickle.load(open('Duration_scaler', 'rb'))
    age_scaler = pickle.load(open('Age_scaler', 'rb'))
    salary_scaler = pickle.load(open('Salary_scaler', 'rb'))
    distance_scaler = pickle.load(open('Distance_scaler', 'rb'))

    industry_array = industry_one_hot.transform([[categories[0]]]).toarray()
    gender_array = gender_one_hot.transform([[categories[1]]]).toarray()
    country_array = country_one_hot.transform([[categories[2]]]).toarray()
    duration_array = duration_scaler.transform(categories[3]).reshape((-1,1))
    age_array = age_scaler.transform(categories[4]).reshape((-1,1))
    salary_array = salary_scaler.transform(categories[5]).reshape((-1,1))
    distance_array = distance_scaler.transform(categories[6]).reshape((-1,1))

    # duration_array = preprocessing.scale(duration_array)
    # age_array = preprocessing.scale(age_array)

    array = np.hstack((industry_array, gender_array, country_array, duration_array, age_array, salary_array, distance_array))

    pred = clf_knn.predict(array)[0]
    con = clf_knn.predict_proba(array)[0,1]
    log_con = clf_knn.predict_log_proba(array)[0,1]
    return pred, con, log_con
